var fs = require('fs');
var youtubedl = require('youtube-dl');
var path = require('path');

var appRouter = function(app) {

    app.get("/mp3/download", function(req, res) {
        
        const basicYouTubeURL = 'http://www.youtube.com/watch?v=';
        // const videoId = req.body.videoId;    // for POST request
        const videoId = req.query.videoid;      // for GET request

        console.log('started!!!', videoId);
        youtubedl.exec(basicYouTubeURL + videoId, ['-x', '--audio-format', 'mp3'], {}, function(err, output) {
            if (err) {
                console.log('err ==', err);
                res.send('error is occured!!!', err);
            }
            else {
                const key = "[ffmpeg] Destination: ";
                let filename = "";
                for (let i = 0; i < output.length; i++) {
                    if (output[i].indexOf(key) >= 0) {
                        filename = output[i].substr(key.length);
                        break;
                    }
                }

                const filePath = path.join(__dirname, '../' + filename);
                // const stat = fs.statSync(filePath);
                
                // res.writeHead(200, {
                //     'Content-Type': 'audio/mp3',
                //     'Content-Length': stat.size
                // });
            
                // var readStream = fs.createReadStream(filePath);
                // // We replaced all the event handlers with a simple call to readStream.pipe()
                // readStream.pipe(res);

                res.download(filePath, filename, function(err) {
                    console.log('download callback called');
                    if( err ) {
                        console.log('something went wrong');
                    }
                });
            }
        });

    });

}

module.exports = appRouter;